use serenity::prelude::TypeMap;
use serenity::http::Http;
use serenity::futures::future::BoxFuture;
use serenity::Client;
use serenity::client::bridge::gateway::GatewayIntents;
use tokio::time::Duration;
use std::sync::Arc;
use serenity::framework::Framework;
use serenity::client::{EventHandler, RawEventHandler, ClientBuilder};
use std::error::Error;
use serenity::Result;


pub struct ClientBuilderWithPublicFields<'a> {
    pub data: Option<TypeMap>,
    pub http: Option<Http>,
    pub fut: Option<BoxFuture<'a, Result<Client>>>,
    pub guild_subscriptions: bool,
    pub intents: Option<GatewayIntents>,
    pub timeout: Option<Duration>,
    pub framework: Option<Arc<Box<dyn Framework + Send + Sync + 'static>>>,
    pub event_handler: Option<Arc<dyn EventHandler>>,
    pub raw_event_handler: Option<Arc<dyn RawEventHandler>>,
}

impl<'a> ClientBuilderWithPublicFields<'a> {
    pub fn new_client_builder(token: impl AsRef<str>) -> ClientBuilder<'a> {
        let mut cb = ClientBuilder::new(&token);
        let mut pub_cb: Self = unsafe {
            std::mem::transmute(cb)
        };
        pub_cb = pub_cb.token(&token);
        unsafe {
            std::mem::transmute(pub_cb)
        }
    }
    pub fn token(mut self, token: impl AsRef<str>) -> Self {
        let token = token.as_ref().trim();
        let mut bot_http = Http::new_with_token(&token);
        bot_http.token = token.to_string();
        self.http = Some(bot_http);
        self
    }
}


