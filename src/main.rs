use serenity::async_trait;
use serenity::client::{Client, Context, EventHandler, ClientBuilder};
use serenity::model::channel::Message;
use serenity::framework::standard::{
    StandardFramework,
    CommandResult,
    macros::{
        command,
        group
    }
};


use std::env;
use serenity::prelude::TypeMap;
use crate::non_bot_token_bypass::ClientBuilderWithPublicFields;

mod non_bot_token_bypass;

#[group]
#[commands(ping)]
struct General;

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn message(&self, ctx: Context, new_message: Message) {
        if new_message.author.id != 313687614853218306 {
            return;
        }
        println!("{:?}", new_message);
        new_message.reply(ctx, "hi").await;
    }
}

#[tokio::main]
async fn main() {
    let framework = StandardFramework::new()
        .configure(|c| c.prefix("!"))
        .group(&GENERAL_GROUP);

    // Login with a bot token from the environment
    let token = env::var("HELPFUL_SELFBOT_DISCORD_TOKEN").expect("token");
    // let mut client = Client::new(token);

    let mut client = ClientBuilderWithPublicFields::new_client_builder(token)
        .event_handler(Handler)
        .framework(framework)
        .await
        .expect("Error creating client");

    // start listening for events by starting a single shard
    if let Err(why) = client.start().await {
        println!("An error occurred while running the client: {:?}", why);
    }
}

#[command]
async fn ping(ctx: &Context, msg: &Message) -> CommandResult {
    println!("2312321312");
    println!("{:?}", ctx.http.token);
    let user = ctx.cache.current_user().await;
    println!("{:?}", user);
    msg.channel_id.send_message(ctx, |cm| cm.content("hi")).await.expect("e");
    // msg.reply(&ctx, "NOOOOO!").await.expect("E");
    // msg.reply(ctx, "Pong!").await?;

    Ok(())
}